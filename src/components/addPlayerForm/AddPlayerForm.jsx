import React from 'react';
import './AddPlayerForm.scss';

class AddPlayerForm extends React.Component {
  constructor(props) {
    super(props);

    this.onAdd = this.onAdd.bind(this);
  }

  onAdd() {
    let id = this.props.list[this.props.list.length - 1].id + 1;
    let imgUrl = `./images/${document.getElementById('images').value}.jpg`;
    let name = document.getElementById('nameText').value;
    let nickname = document.getElementById('nicknameText').value;

    if (name && nickname) {
      this.props.addPlayer(id, imgUrl, name, nickname);
    } else {
      alert('Fill in all the fields!');
    }
  }

  render() {
    return (
      <div id="addPlayerForm">
        <h1>Add player</h1>
        <div id="box">
          <p> Image :</p>
          <select id="images">
            <option>bear</option>
            <option>deadpool</option>
            <option>lion</option>
            <option>simpson</option>
            <option>venom</option>
          </select>
          <p>Name :</p>
          <input type="text" id="nameText" placeholder="Vladislav" />
          <p>Nickname :</p>
          <input type="text" id="nicknameText" placeholder="_nickname_" />
          <button onsubmit="return false;" onClick={this.onAdd}>
            Add
          </button>
        </div>
      </div>
    );
  }
}

export default AddPlayerForm;
