import React from 'react';
import './Player.scss';

const Player = (props) => {
  const onDeletePlayer = (e) => {
    e.preventDefault();
    
    props.deletePlayer(props.idPl);    
  }

  return (
    <div className="player">
      <li>
        <div>
          <img src={props.imgUrl} alt="Error"></img>
        </div>
        <div className="name-nickname">
          <p className="name">{props.namePl}</p>
          <p className="nickname">{props.nicknamePl}</p>
        </div>
        <form>
          <button
            id={props.idPl}
            className="deleteBtn"
            data-title="Удалить игрока"
            onClick={onDeletePlayer}
          >
            <img src="./images/deleteBtn.png" alt="Error"></img>
          </button>
        </form>
      </li>
    </div>
  );
};

export default Player;
