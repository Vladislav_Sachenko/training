import React from 'react';
import './Players.scss';
import Player from '../player/Player';

class Players extends React.Component {
  componentDidMount() {
    fetch('./data/data.json')
      .then((res) => res.json())
      .then(
        (result) => {
          this.props.setPlayers(result.list);
        },
        (error) => {
          alert('Error');
        }
      );
  }

  render() {
    const items = this.props.list.map((player) => (
      <Player
        idPl={player.id}
        imgUrl={player.imgUrl}
        namePl={player.name}
        nicknamePl={player.nickname}
        deletePlayer={this.props.deletePlayer}        
      />
    ));

    return (
      <div id="players">
        <h1>Players</h1>
        <ul id="list">{items}</ul>
      </div>
    );
  }
}

export default Players;
