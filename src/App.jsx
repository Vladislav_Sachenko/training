import React from 'react';
import { createStore } from 'redux';
import './App.scss';
import rootReducer from './store/reducers';
import { Provider } from 'react-redux';
import PlayersContainer from './containers/PlayersContainer';

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const App = () => {
  return (
    <Provider store = {store}>
      <div id="app">        
        <PlayersContainer />
      </div>
    </Provider>
  );
};

export default App;
