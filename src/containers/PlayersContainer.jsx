import React from 'react';
import Players from '../components/players/Players';
import { connect } from 'react-redux';
import { setPlayers, deletePlayer, addPlayer } from '../store/players/actions';
import AddPlayerForm from '../components/addPlayerForm/AddPlayerForm';

class PlayersContainer extends React.Component {
  render() {
    return (
      <div>
        <AddPlayerForm
          addPlayer={this.props.addPlayer}
          list={this.props.list}
        />
        <Players
          setPlayers={this.props.setPlayers}
          list={this.props.list}
          deletePlayer={this.props.deletePlayer}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    list: state.players.list,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setPlayers: (list) => {
      dispatch(setPlayers(list));
    },
    deletePlayer: (id) => {
      dispatch(deletePlayer(id));
    },
    addPlayer: (id, imgUrl, name, nickname) => {
      dispatch(addPlayer(id, imgUrl, name, nickname));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayersContainer);
