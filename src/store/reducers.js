import { combineReducers } from 'redux';
import { playersReducer } from './players/reducers';

export default combineReducers({
  players: playersReducer,
});
