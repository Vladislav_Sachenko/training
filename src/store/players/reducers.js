import { SET_PLAYERS, DELETE_PLAYER, ADD_PLAYER } from './actions';

const defaultState = {
  list: [],
};

export const playersReducer = (state = defaultState, action) => {
  // eslint-disable-next-line default-case
  switch (action.type) {
    case SET_PLAYERS:
      return {
        ...state,
        list: action.payload.list,
      };
    case DELETE_PLAYER:
      return {
        ...state,
        list: [
          // eslint-disable-next-line eqeqeq
          ...state.list.filter((player) => player.id != action.payload.id),
        ],
      };
    case ADD_PLAYER:
      return {
        ...state,
        list: [...state.list, action.payload.player],
      };
  }

  return state;
};
