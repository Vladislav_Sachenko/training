export const SET_PLAYERS = 'SET_PLAYERS';
export const DELETE_PLAYER = 'DELETE_PLAYER';
export const ADD_PLAYER = 'ADD_PLAYER';

export const setPlayers = (list) => ({
  type: SET_PLAYERS,
  payload: { list },
});

export const deletePlayer = (id) => ({
  type: DELETE_PLAYER,
  payload: { id },
});

export const addPlayer = (id, imgUrl, name, nickname) => ({
  type: ADD_PLAYER,
  payload: { player: { id, imgUrl, name, nickname } },
});
